//
//  ProductsPT.swift
//  WideTechTest
//
//  Created by alejo on 17/10/20.
//

import Foundation

class ProductsPT: ProductsPresenter{
    
    weak var view: ProductsView?
    
    init(view: ProductsView){
        self.view = view
    }
}
