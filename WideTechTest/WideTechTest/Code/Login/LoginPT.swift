//
//  LoginPT.swift
//  WideTechTest
//
//  Created by alejo on 17/10/20.
//

import Foundation

class LoginPT: LoginPresenter{
    
    weak var view: LoginView?
    
    init(view: LoginView){
        self.view = view
    }
}
