//
//  ContactPT.swift
//  WideTechTest
//
//  Created by alejo on 17/10/20.
//

import Foundation

class ContactPT: ContactPresenter{
    
    weak var view: ContactView?
    
    init(view: ContactView){
        self.view = view
    }
}
