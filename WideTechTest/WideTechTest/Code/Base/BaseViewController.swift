//
//  BaseViewController.swift
//  WideTechTest
//
//  Created by alejo on 17/10/20.
//

import UIKit

/// Clase Base de ViewControllers para implemetar comportamientos comunes (como el cargando en este ejemplo) a todos los viewcontrollers de la app. Tambien se podrian implementar atravez de extensiones.
class BaseViewController: UIViewController {

    var spinnerView : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Podria solo instanciarse cuando se necesite
        
    }
    
    private func createSpinner(){
        spinnerView = UIView.init(frame: view.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.large)
        ai.startAnimating()
        ai.center = spinnerView.center
        self.spinnerView.addSubview(ai)
    }
}

extension BaseViewController: BaseIView{
    func displaySpinner() {
        DispatchQueue.main.async {
            self.view.addSubview(self.spinnerView)
        }
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.spinnerView.removeFromSuperview()
        }
    }
}
