//
//  BaseIView.swift
//  WideTechTest
//
//  Created by alejo on 17/10/20.
//

import Foundation

/// Interfaz Base para los BaseViewControllers
protocol BaseIView : class{
    func displaySpinner()
    func removeSpinner()
}
